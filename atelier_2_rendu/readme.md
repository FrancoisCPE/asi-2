# Atelier ASI-2

Nasri Galdeano - Thomas Oudard - François Portaz - Antonin Poulard 

## Suivi des scéances

|Séances|Nasri                              |Thomas                                     |François                           |Antonin                         |
|:-----:|:---------------------------------:|:-----------------------------------------:|:---------------------------------:|:------------------------------:|
| atelier 1                                                                                                                                                  |
|28/09  |Diagramme de classe monolithique   |Diagramme de classe monolithique           |Diagramme de classe monolithique   |Diagramme de classe monolithique|
|29/09  |Architecture micro-service         |Architecture micro-service                 |Architecture micro-service         |Architecture micro-service      |
|15/11  |Front - ReactJS                    |Docker-compose                             |Librairies DTO                     | Tableau comparatif des framwork|
| atelier 2                                                                                                                                                  |
|16/11  |Front - ReactJS                    |Recherche Nexus3 + Aide François           |Librairies DAO                     | Mise en place du chat          |
|17/11  |Front - ReactJS                    |Création du micro-service auth             |Modification des classes existantes|Mise en place de l'ESB          |
|18/11  |Front - ReactJS                    |Création du micro-service auth             |Modification des classes existantes|Mise en place de l'ESB          |
|13/12  |Absent                             |Absent  |Finalisation du service Store + connexion front-back|Mise en place de l'ESB                            |
|13/12  |Connexion front-back + aide Thomas |pipeline CI/CD                             |Correction de bugs sur les services|Programmation du chat Node.js   |
| atelier 3                                                                                                                                                  |
|15/12  |Connexion front-back + aide Thomas |pipeline CI/CD  (+test covid cas contact)  |Correction de bug spring-boot      |Programmation du chat Node.js   |

## Éléments réalisés

|Éléments           |Réalisé                                                                                                   |
|:-----------------:|:--------------------------------------------------------------------------------------------------------:|
|Docker             |Terminé mais pas utilisé                                                                                  |
|CI/CD              |Terminé et utilisé pour le build des DAO et DTO et deploy sur package registry gitlab                     |
|CI/CD              |Mise en place OK pour fichiers de conf -> Token CICD n'a pas les droits en écriture sur le repo d'images  |
|Front              |Terminé pour l'authentification, l'affichage des cards                                                    |
|Micro-service Auth |Terminé mais pas de lien avec le service user                                                             |
|Micro-service User |Terminé (mais bdd h2)                                                                                     |
|Micro-service Card |Terminé (mais bdd h2)                                                                                     |
|Micro-service Store|Terminé mais pas testé                                                                                    |
|Node.js            |Chat général (discussion avec tous les users)                                                             |
|ESB                |Codé mais pas lié                                                                                         |
## Vidéos youtube

[S'inscrire puis se connecter](https://www.youtube.com/watch?v=zdGlnFQfFJ0)


[Afficher les cards](https://www.youtube.com/watch?v=sPPYtAtqrmc)

## Éléments non-réalisés

|Éléments            |
|:------------------:|
|Combats             |
|Chat perso          |

## Problèmes bloquants du projet
 - Les modifications sur application.properties ne semblent pas être prises en compte. Problème malheureusement détécté à une date proche de la deadline. Ceci nous a fait perdre beaucoup de temps notamment dans la mise en oeuvre de la connexion avec une base de données postgresql et non h2.

 - Le push des images sur le repository gitlab ne fonctionne pas du à un problème de droit inexpliquable. L'utilisation des variables d'environnement gitlab (job CI/CD token) ne semble pas fonctionner, problème d'écriture. De même avec un token personnel et un deploy token.

~~~
 denied: requested access to the resource is denied
~~~





## Liens utile

[Gitlab repo](https://gitlab.com/FrancoisCPE/asi-2/-/tree/main)
