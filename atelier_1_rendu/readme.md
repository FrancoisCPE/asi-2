# Atelier ASI-2

Nasri Galdeano - Thomas Oudard - François Portaz - Antonin Poulard 

## Suivi des scéances

|Séances|Nasri                           |Thomas                          |François                           |Antonin                         |
|:-----:|:------------------------------:|:------------------------------:|:---------------------------------:|:------------------------------:|
|28/09  |Diagramme de classe monolithique|Diagramme de classe monolithique|Diagramme de classe monolithique   |Diagramme de classe monolithique|
|29/09  |Architecture micro-service      |Architecture micro-service      |Architecture micro-service         |Architecture micro-service      |
|15/11  |Front - ReactJS                 |Docker-compose                  |Librairies DTO                     | Tableau comparatif des framwork|
|16/11  |Front - ReactJS                 |Recherche Nexus3 + Aide François|Librairies DAO                     | Mise en place du chat          |
|17/11  |Front - ReactJS                 |Création du micro-service auth  |Modification des classes existantes|Mise en place de l'ESB          |
|18/11  |Front - ReactJS                 |Création du micro-service auth  |Modification des classes existantes|Mise en place de l'ESB          |

## Suivi des éléments

|Éléments           |Réalisé                                                                                |
|:-----------------:|:-------------------------------------------------------------------------------------:|
|Docker             |Oui                                                                                    |
|Front              |En cours de finalisation (components créés mais toutes les pages ne sont pas terminées)|
|Micro-service Auth |En cours de finalisation (Ajout des DTO + Liens ESB)                                   |
|Micro-service User |En cours (Liens ESB à ajouter)                                                         |
|Micro-service Card |En cours (Liens ESB à ajouter)                                                         |
|Micro-service Store|En cours (Liens ESB à ajouter)                                                         |
|ESB                |Oui(sauf pour Store)                                                                   |

## Liens utile

[Gitlab](https://gitlab.com/FrancoisCPE/asi-2)
