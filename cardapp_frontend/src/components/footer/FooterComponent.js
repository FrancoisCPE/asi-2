import { RiGitlabFill } from 'react-icons/ri';

export const FooterComponent = () => {
    return (
        <div className="mb-3">
            <hr/>
            <div>
                CPE - Projet ASI 2 - Rattrapages
            </div>
            <div>
                GALDEANO Nasri | OUDARD Thomas | PORTAZ François | POULARD Antonin
            </div>
            <div>
                <RiGitlabFill size={20} />
                &nbsp;
                <a href="https://gitlab.com/FrancoisCPE/asi-2" target="_blank" rel="noreferrer">GitLab</a>
                &nbsp;
                <RiGitlabFill size={20} />
            </div>
        </div>
    )
}