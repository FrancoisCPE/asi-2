import { useState } from "react"
import UserService from "../../services/UserService"
import { UserRegister } from "../../model/UserRegister"

import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import { useHistory } from "react-router"


export const UserFormRegister = (props) => {

    const history = useHistory()
    const [login, setLogin] = useState(props.login)
    const [password, setPassword] = useState(props.password)

    const [surName, setSurName] = useState(props.surName)
    const [lastName, setLastName] = useState(props.lastName)
    
    const [email, setEmail] = useState(props.description)

    const handleSubmit = (e) => {
        e.preventDefault()
        let user = new UserRegister(login, password, surName, lastName, email, 5000)
        console.log(user);
        UserService.getInstance().addUser(user).then(function(response) {
            if(response.status === 200) {
                history.push('login')
            } else {
                alert("Cela n'a pas fonctionné...")
            }
        })
    }

    const handleInputChange = (event) => {
        const value = event.target.value
        const name = event.target.name

        switch(name) {
            case "surName":
                setSurName(value)
                break
            case "lastName":
                setLastName(value)
                break
            case "email":
                setEmail(value)
                break
            case "login":
                setLogin(value)
                break
            case "password":
                setPassword(value)
                break
            default: 
                break;
        }
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            handleSubmit()
        }
    }

    return (
        <Form onSubmit={handleSubmit} onKeyPress={handleKeyPress}>
            <Form.Group className="m-3">
                <Form.Label>
                    Pseudo
                </Form.Label>
                <Form.Control
                    name="login"
                    type="text"
                    placeholder="Pseudo"
                    required
                    value={login}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Mot de passe
                </Form.Label>
                <Form.Control
                    name="password"
                    type="password"
                    placeholder="Mot de passe"
                    required
                    value={password}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Nom
                </Form.Label>
                <Form.Control
                    name="lastName"
                    type="text"
                    placeholder="Nom"
                    required
                    value={lastName}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Prénom
                </Form.Label>
                <Form.Control
                    name="surName"
                    type="text"
                    placeholder="Prénom"
                    required
                    value={surName}
                    onChange={handleInputChange} />
            </Form.Group>
            <Form.Group className="m-3">
                <Form.Label>
                    Email
                </Form.Label>
                <Form.Control
                    name="email"
                    type="text"
                    placeholder="Email"
                    value={email}
                    onChange={handleInputChange} />
            </Form.Group>
            
            
            <Button
                className="m-3"
                variant="primary"
                type="submit">
                Créer le compte
            </Button>
        </Form>
    )
}