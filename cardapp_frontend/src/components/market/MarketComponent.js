import Container from 'react-bootstrap/Container'
import { Row, Col, Button } from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { useLocation } from 'react-router-dom';
import {StoreModel} from '../../model/StoreModel'


import CardService from '../../services/CardService'
import StoreService from '../../services/StoreService'
import { useEffect, useState } from 'react'

import { useSelector } from 'react-redux'
import { selectSelectedCard, selectUser } from '../../core/selectors'
import {GameCardList} from '../cards/GameCardList'
import {GameCard} from '../cards/GameCard'

import { setSelectedCard } from '../../core/actions'

export const MarketComponent = () => {

    const location = useLocation();

    const user = useSelector(selectUser)
    const selectedCard = useSelector(selectSelectedCard)
    const dispatch = useDispatch()

    const [cards, setCards] = useState([])
    const [userId,setUserId] = useState()
    // const [query, setQuery] = useState()

    useEffect(() => {
        setUserId(user.id)
        const userIdToUse = (location.pathname === '/buy') ? 0: user.id
        CardService.getInstance().getCardsByUserId(userIdToUse).then((response) => {
            response.json().then((value) => {
                setCards(value)
            })
        })
    }, [])

    const handleMarketAction = (e) => {
        e.preventDefault()
        let storeExchange = new StoreModel(userId, selectedCard.id)
        if ( location.pathname === '/buy'){
            StoreService.getInstance().buyCard(storeExchange).then(function(response) {
                response.json()
                .then(value => {
                    console.log(value)
                    if(response.status === 200 && value === true) {
    
                        window.location.reload(false)
                    } else {
                        alert("Vous n'avez pas assez de MONEYYYY")
                    }
                })
                
                
            })
        } else {
            StoreService.getInstance().sellCard(storeExchange).then(function(response) {
                response.json()
                .then(value => {
                    console.log(value)
                    if(response.status === 200 && value === true) {
                        window.location.reload(false)
                    } else {
                        alert("Erreur durant la transaction")
                    }
                })
                
                
            })

        }
        
    }

    const onSelectInGameCardList = (card) => {
        dispatch(setSelectedCard(card))
    }

    return (
        <Container>
            {console.log(selectedCard)}
            <h1>Marketplace</h1>
            <h2>{
            location.pathname === '/buy' ? 'Achat': 'Vente'
        }</h2>
            <Row>
                <Col md={6} lg={8}>
                    <GameCardList 
                    cards={cards}
                    visual_type="list_group"
                    onClickOnCard={onSelectInGameCardList}
                    toSelect={true}
                    />
                </Col>
                <Col md={6} lg={4}>
                    <GameCard 
                            visual_type="detailed"
                            card={selectedCard} 
                        />
                        {selectedCard.id ?
                        <div className="d-grid gap-2">
                            <Button onClick={handleMarketAction} variant="primary" size="lg">
                                {location.pathname === '/buy' ? 'Acheter': 'Vendre'}
                                <span> ({selectedCard.price})</span>
                            </Button>
                        </div>
                        : '' }
                </Col>
            </Row>
            
        </Container>
    )
}