/* eslint-disable react/jsx-pascal-case */
import { Spinner, Alert } from 'react-bootstrap'
import {GameCardList_asDeck} from './visual/GameCardList_asDeck'
import {GameCardList_asTable} from './visual/GameCardList_asTable'



export const GameCardList = props => {

  const {cards, toSelect, onClickOnCard, visual_type} = props

  if (cards.length < 1) {
    return <Spinner animation='border' />
  }

  switch (visual_type){
    case("list_group") :
      return (
        <div>    
            {
                <GameCardList_asTable
                  cards={cards}
                  toSelect={toSelect}
                  onClickOnCard={onClickOnCard}
                />
              }
        </div>);
    case("deck_group") :
      return (
      <div>    
          {
              <GameCardList_asDeck 
                cards={cards}
                toSelect={toSelect}
                onClickOnCard={onClickOnCard}
              />
            }
      </div>);
    default :
    return (
      <Alert bsStyle="warning">
        Your card list could not be displayed !<br/><strong>DISPLAY_STYLE_INVALID</strong>
      </Alert>
    )}
  }
