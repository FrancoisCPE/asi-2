/* eslint-disable react/jsx-pascal-case */
import { Card, Alert } from "react-bootstrap";
import { useSelector } from "react-redux";
import { GameCard_asRow } from "./visual/GameCard_asRow";
import { GameCard_asDetailed } from "./visual/GameCard_asDetailed";
import { selectSelectedCard } from "../../core/selectors";



export const GameCard = props => {
    const selectedCard = useSelector(selectSelectedCard)
    
    const {visual_type, toSelect, card, handleOnClick} = props
    
    let handle_fct = () => {
        handleOnClick(card)
    }

    let isSelected = selectedCard.id === card.id;

    if(!toSelect)
    {
        handle_fct = null;
        isSelected = false;
    }
    else if (!toSelect && !selectedCard.id) {
        return (
            <Card >
                <Card.Body>
                    Select a card to see it here !
                </Card.Body>
            </Card>
        )
    }
    switch (visual_type){
        case("in_row") :
            return (
                    
                    <GameCard_asRow
                            card={card}
                            onClick={handle_fct}
                            isSelected={isSelected}
                        />
                    
                );
            case("detailed") :
            return (
                <div>    
                    {<GameCard_asDetailed
                            card={card}
                            onClick={handle_fct}
                            isSelected={isSelected}
                        />
                    }
                </div>);
        default :
            return (
            <Alert bsStyle="warning">
                Your card can not be displayed !<br/><strong>DISPLAY_STYLE_INVALID</strong>
            </Alert>
            )
    }
}
