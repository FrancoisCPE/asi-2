import { Card, Image } from "react-bootstrap";
import { Label } from "../Label";



export const GameCard_asDetailed = props => {
    const {card, onClick, isSelected} = props
    return (
        <Card onClick={onClick} bg={isSelected ? "primary" : "default"}>
            <Card.Header>
                <Card.Title>Card {card.id} description</Card.Title>
            </Card.Header>
            <Card.Body>
                <Label title={card.name} id={card.id} />
                <Image
                    src={card.smallImgUrl}
                    roundedCircle
                />
            </Card.Body>
        </Card>
    )
}
