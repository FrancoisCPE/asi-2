export const GameCard_asRow = props => {
    const {card, onClick, isSelected} = props
    return (
        <tr onClick={onClick} className={isSelected ? "table-active" : ""}>
            <td >{card.name}</td>
            <td >{card.description}</td>
            <td >{card.family}</td>
            <td >{card.affinity}</td>
            <td >{card.energy}</td>
            <td >{card.hp}</td>
            <td >{card.price}</td>
        </tr>
    )
}

