import { GameCard } from '../GameCard'

export const GameCardList_asDeck = props => {

    const {cards, toSelect, onClickOnCard} = props

    return (
    <div>    
        {cards.map(
            card =>
            <GameCard
                visual_type="detailed"
                toSelect={toSelect}
                card={card} 
                handleOnClick={onClickOnCard}
            />
          )}
    </div>)
}