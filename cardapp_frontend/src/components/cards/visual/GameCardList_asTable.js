import { Table } from 'react-bootstrap'
import { GameCard } from '../GameCard'

export const GameCardList_asTable = props => {

  const {cards, toSelect, onClickOnCard} = props

  return (
      <div>
        <Table responsive className="table table-hover"> 
          <thead>
            <tr>
              <th>Name</th>
              <th>Description</th>
              <th>Family</th>
              <th>Affinity</th>
              <th>Energy</th>
              <th>HP</th>
              <th>Price</th>
            </tr>
        </thead>
        <tbody>
        {cards.map(
          card =>
          <GameCard
              visual_type="in_row"
              toSelect={toSelect}
              card={card}
              handleOnClick={onClickOnCard}
          />
        )}
    
        </tbody>
      </Table>;
      </div>
  )
}