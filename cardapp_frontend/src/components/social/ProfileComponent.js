import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'

import { useEffect, useState } from 'react'
import { useParams } from 'react-router'

import UserService from '../../services/UserService'
import { useSelector } from 'react-redux'
import { selectUser } from '../../core/selectors'
import { UserDisplay } from '../../model/UserDisplay'

import { useDispatch } from 'react-redux'
import { setUser } from '../../core/actions'


export const ProfileComponent = () => {

    const user = useSelector(selectUser)
    console.log(user)
    const [login, setLogin] = useState()
    const [surName, setSurName] = useState()
    const [lastName, setLastName] = useState()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [account, setAccount] = useState()


    const dispatch = useDispatch()

    const userId = useParams().id

    useEffect(() => {
            setSurName(user.surName)
            setLastName(user.lastName)
            setEmail(user.email)
            setLogin(user.login)
            setPassword(user.password)
            setAccount(user.account)
        }, [])

    const handleSubmit = (e) => {
        e.preventDefault()
        let user = new UserDisplay(userId, login, surName, lastName, email, password, account)

        UserService.getInstance().updateUser(user).then(function(response) {
            if(response.status === 200) {
                dispatch(setUser(user))
                window.location.reload(false)
            } else {
                alert("Saisie incorrect")
            }
            
        })
    }

    const handleInputChange = (event) => {
        const value = event.target.value
        const name = event.target.name

        switch(name) {
            case "surName":
                setSurName(value)
                break
            case "lastName":
                setLastName(value)
                break
            case "email":
                setEmail(value)
                break
            case "password":
                setPassword(value)
                break
            default: 
                break
        }
    }

    const handleKeyPress = (e) => {
        if (e.key === 'Enter') {
            handleSubmit()
        }
    }

    return (
        <Container>
            <h2>Profil de { user.surName } { user.lastName }</h2> 
            <Form onSubmit={handleSubmit} onKeyPress={handleKeyPress}>
                <Form.Group className="m-3">
                    <Form.Label>
                        Prénom
                    </Form.Label>
                    <Form.Control
                        name="surName"
                        type="text"
                        placeholder="Prénom"
                        required
                        value={surName}
                        onChange={handleInputChange} />
                </Form.Group>
                <Form.Group className="m-3">
                    <Form.Label>
                        Nom
                    </Form.Label>
                    <Form.Control
                        name="lastName"
                        type="text"
                        placeholder="Nom"
                        required
                        value={lastName}
                        onChange={handleInputChange} />
                </Form.Group>
                <Form.Group className="m-3">
                    <Form.Label>
                        Email
                    </Form.Label>
                    <Form.Control
                        name="email"
                        type="text"
                        placeholder="email"
                        value={email}
                        onChange={handleInputChange} />
                </Form.Group>
                <Form.Group className="m-3">
                    <Form.Label>
                        Nom d'utilisateur
                    </Form.Label>
                    <Form.Control
                        name="login"
                        type="text"
                        placeholder="Nom d'utilisateur"
                        required
                        value={login}
                        disabled= {true}
                        onChange={handleInputChange}/>
                </Form.Group>
                    <Form.Group className="m-3">
                        <Form.Label>
                            Mot de passe
                        </Form.Label>
                        <Form.Control
                            name="password"
                            type="password"
                            placeholder="Mot de passe"
                            required
                            value={password}
                            onChange={handleInputChange} />
                    </Form.Group>
                
                 <Button className="m-3" type="submit">Enregistrer</Button> 
            </Form>
        </Container>
    )
}