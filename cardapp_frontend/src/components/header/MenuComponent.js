import Nav from 'react-bootstrap/Nav'

import { useSelector } from 'react-redux'
import { selectUser } from '../../core/selectors'

export const MenuComponent = () => {

    const userId = useSelector(selectUser).id
    const hrefProfile = `/profile/${userId}`

    return (
        <h4>
            <Nav justify>
                <Nav.Item>
                    <Nav.Link href="/">Jouer</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/buy">Acheter</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/sell">Vendre</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href={hrefProfile}>Profil</Nav.Link>
                </Nav.Item>
            </Nav>
        </h4>
    )
}