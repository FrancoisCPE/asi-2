import { GiCardJoker } from 'react-icons/gi'
import { GrMoney } from 'react-icons/gr'
import { FaUser } from 'react-icons/fa'
import UserService from '../../services/UserService'

import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { MenuComponent } from './MenuComponent'
import { Button } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux'
import { selectUser, selectIsLogged } from '../../core/selectors';
import { setLogoutUser } from '../../core/actions';
import { useEffect } from 'react'

import { useHistory } from 'react-router';
import { setUser } from '../../core/actions'

export const HeaderComponent = () => {

    const user = useSelector(selectUser)
    const isLogin = useSelector(selectIsLogged)
    const dispatch = useDispatch()
    const history = useHistory();

    const handleLogout = () => {
        dispatch(setLogoutUser())
    }

    const handleLogin = () => {
        history.push('login')
    }

    const handleRegister = () => {
        history.push('register')
    }

    useEffect(() => {
        const userId = user.id
        UserService.getInstance().getUser(userId).then((response) => {
            response.json().then((value) => {
                dispatch(setUser(value))
            })
        })
    }, [])

    return (
        <div>
            <Row className="align-items-center">
                <Col className="mt-3">
                    <div>
                        <h1><GiCardJoker />&nbsp;POKAMON&nbsp;<GiCardJoker /></h1>
                    </div>
                    <div>
                        <b><i>Jeu de carte amusant</i></b>
                    </div>
                </Col>
                <Col>
                    <MenuComponent></MenuComponent>
                </Col>
                <Col>
                    { isLogin ? (
                        <div>
                            <h1>{user.firstName} {user.login}&nbsp;<FaUser /></h1>
                            <h1>{user.account} &nbsp;<GrMoney /></h1>
                            <Button onClick={handleLogout} variant="outline-danger">
                                Se déconnecter
                            </Button>
                        </div>
                    ) : (
                        <div>
                            <Button onClick={handleRegister}>
                                Créer un compte
                            </Button>
                            &nbsp;
                            <Button onClick={handleLogin} variant="outline-primary">
                                Se connecter
                            </Button>
                        </div>
                    )}
                </Col>
            </Row>
            <hr/>
        </div>
    )
}