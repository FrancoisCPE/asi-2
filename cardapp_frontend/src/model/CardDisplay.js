export class CardDisplay {
    constructor(id, name, description, family, affinity, imgUrl,smallImgUrl, energy, hp, defence, attack, price, userId) {
        this.id = id
        this.name = name;
        this.description = description;
        this.family = family;
        this.affiniy = affinity;
        this.imgUrl = imgUrl;
        this.smallImgUrl = smallImgUrl;
        this.energy = energy;
        this.hp = hp;
        this.defence = defence;
        this.attack = attack;
        this.price = price;
        this.userId = userId;
    }
}