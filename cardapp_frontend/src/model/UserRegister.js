export class UserRegister {
    constructor(login, password, surName, lastName, email, account) {
        this.login = login;
        this.password = password;
        this.surName = surName;
        this.lastName = lastName;
        this.email = email;
        this.account = account;
    }
}