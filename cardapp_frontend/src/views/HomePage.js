import Container from 'react-bootstrap/Container'
import { Row, Col } from 'react-bootstrap'
import { useDispatch } from 'react-redux'

import CardService from '../services/CardService'
import { useEffect, useState } from 'react'

import { useSelector } from 'react-redux'
import { selectSelectedCard, selectUser } from '../core/selectors'
import {GameCardList} from '../components/cards/GameCardList'


export const HomePage = () => {

    const user = useSelector(selectUser)
    const selectedCard = useSelector(selectSelectedCard)

    const [cards, setCards] = useState([])
    // const [query, setQuery] = useState()

    useEffect(() => {
        CardService.getInstance().getCardsByUserId(user.id).then((response) => {
            response.json().then((value) => {
                setCards(value)
            })
        })
    }, [])

    return (
        <Container>
            {console.log(selectedCard)}
            <h1>Mon Deck</h1>
            <Row>
                <Col md={6} lg={8}>
                    <GameCardList 
                    cards={cards}
                    visual_type="deck_group"
                    toSelect={false}
                    />
                </Col>
            </Row>
            
        </Container>
    )
}