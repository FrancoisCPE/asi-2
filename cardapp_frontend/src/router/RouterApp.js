import { Switch } from "react-router-dom"
import { Route } from "react-router"

import { PrivateRoute } from "./PrivateRoute"
import { PublicRoute } from "./PublicRoute"

import { HomePage } from "../views/HomePage"
import { MarketPage } from "../views/MarketPage"
import { ProfilePage } from "../views/ProfilePage"
import { LoginPage } from "../views/LoginPage"
import { RegisterPage } from "../views/RegisterPage"

export const RouterApp = () => {
    return (
        <Switch>
            <PrivateRoute exact component={HomePage} path="/" />
            <PrivateRoute exact component={MarketPage} path="/buy" />
            <PrivateRoute exact component={MarketPage} path="/sell" />
            <PrivateRoute component={ProfilePage} path="/profile/:id" />
            <PublicRoute exact component={LoginPage} path="/login"/>
            <PublicRoute exact component={RegisterPage} path="/register"/>
            <Route><h1>404 Not Found</h1></Route>
        </Switch>
    );
}