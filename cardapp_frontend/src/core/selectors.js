export const selectIsLogged = state => state.userState.isLogged
export const selectUser = state => state.userState.user
export const selectCards = state => state.cardState.cards
export const selectSelectedCard = state => state.cardState.selectedCard
