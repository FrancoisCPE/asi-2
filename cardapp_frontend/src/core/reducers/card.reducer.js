import { UPDATE_SELECTED_CARD, UPDATE_CARDS } from '../actions';

const initialState = {
    selectedCard: {},
    cards : []
}

const cardReducer = (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_SELECTED_CARD:
            return {
                ...state,
                selectedCard: action.payload,
            }
        case UPDATE_CARDS:
            return {
                ...state,
                cards: action.payload
            }
        default:
            return state
    }
}

export default cardReducer