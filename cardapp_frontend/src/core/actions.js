import { UserDisplay } from '../model/UserDisplay';
export const UPDATE_ISLOGGED = 'UPDATE_ISLOGGED'
export const UPDATE_USER = 'UPDATE_USER'
export const LOGOUT_USER = 'LOGOUT_USER'

export const UPDATE_SELECTED_CARD = 'UPDATE_SELECTED_CARD'
export const UPDATE_CARDS = 'UPDATE_CARDS'


export function setIsLogged(value) {
    return { 
        type: UPDATE_ISLOGGED, 
        payload: value 
    }
}

export function setUser(user) {
    return { 
        type: UPDATE_USER, 
        payload: user 
    }
}

export function setLogoutUser() {
    return { 
        type: LOGOUT_USER, 
        payload: {
            isLogged: false,
            user: new UserDisplay()
        }
    }
}

export const setSelectedCard = (card) => ({
    type: UPDATE_SELECTED_CARD,
    payload: card,
})

export const updateCards = (cards) => ({
    type: UPDATE_CARDS,
    payload: cards,
})
