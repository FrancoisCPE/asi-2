import { Service } from './Service'

export class CardService extends Service {

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new CardService()
        }

        return this.instance
    }
    
    constructor() {
        // Notre serveur.
        const domain = "http://127.0.0.1:82/api"

        super(domain)
    }


    getCard(id) {
        const url = `card/${id}`
        const method = "GET"

        return super.request(url, method)

    }
    getCardsByUserId(userId) {
        const url = `cards_user/${userId}`
        const method = "GET"

        return super.request(url, method)
    }

}

export default CardService