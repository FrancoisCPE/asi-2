import { Service } from './Service'

export class StoreService extends Service {

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new StoreService()
        }

        return this.instance
    }
    
    constructor() {
        // Notre serveur.
        const domain = "http://127.0.0.1:83/api"

        super(domain)
    }


    buyCard(storeModel) {
        const url = `buy/`
        const method = "POST"
        const data = storeModel
        return super.request(url, method, data)

    }
    sellCard(storeModel) {
        const url = `sell/`
        const method = "POST"
        const data = storeModel
        return super.request(url, method, data)

    }

}

export default StoreService