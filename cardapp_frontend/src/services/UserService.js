import { Service } from './Service'

export class UserService extends Service {

    static instance = null

    static getInstance() {
        if(this.instance == null) {
            this.instance = new UserService()
        }

        return this.instance
    }
    
    constructor() {
        // Notre serveur.
        const domain = "http://127.0.0.1:84/api"

        super(domain)
    }

    authenticate(userLogin) {
        const url = `user/auth`
        const method = "POST"
        const data = userLogin

        super.request(url, method, data)
    }

    getUser(id) {
        const url = `user/${id}`
        const method = "GET"

        return super.request(url, method)
        
    }

    addUser(user) {
        const url = `user`
        const method = "POST"
        const data = user

        return super.request(url, method, data)
    }

    updateUser(user){
        const url = `user/${user.id}`
        const method = "PUT"
        const data = user

        return super.request(url, method, data)
    }

    deleteUser(id) {
        const url = `user/${id}`
        const method = "DELETE"

        return super.request(url, method)
    }
}

export default UserService