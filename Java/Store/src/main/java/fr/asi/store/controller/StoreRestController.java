package fr.asi.store.controller;

import fr.asi.store.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.asi.dto.StoreDTO;

@CrossOrigin
@RestController
public class StoreRestController {

	@Autowired
	private StoreService storeService;

	@RequestMapping(method = RequestMethod.POST, value = "/api/buy")
	private boolean buyCard(@RequestBody StoreDTO order) {
		return storeService.buyCard(order.getUserId(), order.getcardId());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/sell")
	private boolean sellCard(@RequestBody StoreDTO store) {
		return storeService.sellCard(store.getUserId(), store.getcardId());
	}
}
