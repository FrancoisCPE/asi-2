package fr.asi.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppStore {
	public static void main(String[] args) {
		SpringApplication.run(AppStore.class, args);
	}
}
