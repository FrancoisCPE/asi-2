package fr.asi.store.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import fr.asi.dao.impl.CardImpl;
import fr.asi.dao.impl.UserImpl;
import fr.asi.dto.CardDTO;
import fr.asi.dto.UserDTO;

import java.util.Objects;

@Service
public class StoreService {
	private CardImpl cardDAO = new CardImpl();
	private UserImpl userDAO = new UserImpl();

	public StoreService() {
	}

	public StoreService(CardImpl cardDAO, UserImpl userDAO) {
		this.cardDAO = cardDAO;
		this.userDAO = userDAO;
	}

	public boolean buyCard(Integer userId, Integer cardId) {
		UserDTO userDTO = userDAO.getUser(userId).getBody();
		CardDTO cardDTO = cardDAO.getCard(cardId).getBody();

		assert cardDTO != null;
		float price = cardDTO.getPrice();

		assert userDTO != null;
		float account = userDTO.getAccount();

		if (price < account) {
			float balance = account - price;

			userDTO.setAccount(balance);
			userDAO.updateUser(userDTO, userDTO.getId());

			cardDTO.setUserId(userDTO.getId());
			cardDAO.updateCard(cardDTO, cardDTO.getId());

			return true;
		}
		return false;
	}

	public boolean sellCard(Integer userId, Integer cardId) {
		UserDTO userDTO = userDAO.getUser(userId).getBody();
		CardDTO cardDTO = cardDAO.getCard(cardId).getBody();

		assert cardDTO != null;
		float price = cardDTO.getPrice();

		assert userDTO != null;
		float account = userDTO.getAccount();

		float balance = account + price;

		userDTO.setAccount(balance);
		userDAO.updateUser(userDTO, userDTO.getId());

		cardDTO.setUserId(0);
		cardDAO.updateCard(cardDTO, cardDTO.getId());

		return true;
	}
}
