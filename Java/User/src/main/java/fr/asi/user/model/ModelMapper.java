package fr.asi.user.model;

import org.springframework.stereotype.Service;

import fr.asi.dto.UserDTO;
import fr.asi.user.model.UserModel;

@Service
public class ModelMapper {

	public UserModel dtoToModel(UserDTO userDTO) {
		return new UserModel(userDTO.getId(),userDTO.getLogin(),"",userDTO.getAccount(),userDTO.getLastName(),userDTO.getSurName(),userDTO.getEmail());
	}
	
	public UserDTO modelToDto(UserModel userModel) {
		return new UserDTO(userModel.getId(),userModel.getLogin(),userModel.getAccount(),userModel.getLastName(),userModel.getSurName(),userModel.getEmail());
	}
}

