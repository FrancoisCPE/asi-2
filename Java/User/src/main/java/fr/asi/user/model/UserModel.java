package fr.asi.user.model;

import org.apache.catalina.User;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.*;
import javax.swing.*;

@Entity
@Table(name = "USERS")
//@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class UserModel implements Serializable {

	private static final long serialVersionUID = 2733795832476568049L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	@Column(unique=true)
	private String login;
	private String password;
	private float account;
	private String lastName;
	private String surName;
	private String email;


	public UserModel() {
	}

	public UserModel(String login, String password){
		this.login = login;
		this.password = password;
	}

	public UserModel(Integer id, String login, String password) {
		this.id= id;
		this.login = login;
		this.password = password;
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public UserModel(Integer id, String login, String password, float account, String lastName, String surName, String email) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.account = account;
		this.lastName = lastName;
		this.surName = surName;
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public float getAccount() {
		return account;
	}

	public void setAccount(float account) {
		this.account = account;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
