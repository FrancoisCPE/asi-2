package fr.asi.user.servicebus;

import fr.asi.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import fr.asi.user.model.UserEnveloppe;
import fr.asi.user.model.UserModel;


@Component
public class UserSBListener {
	
	private static final String queue = "Endpoint=sb://asi2groupe5.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=flrDi447tRkVp2X24LaJEaU/sg9uwaG9zFXmA3tYGqM=";
	
	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	private UserService userService;
	

    @JmsListener(destination = queue, containerFactory = "connectionFactory")
    public void receiveAddUser(UserEnveloppe enveloppe) {
        System.out.println(enveloppe);
        if (enveloppe.getAction().equals("addUser")) {
            UserModel userModel = enveloppe.getUser();
            userService.addUser(userModel);
        }
    }

    @JmsListener(destination = queue, containerFactory = "connectionFactory")
    public void receiveUpdateUser(UserEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("updateUser")) {
            UserModel userModel = enveloppe.getUser();
            userService.updateUser(userModel);
        }
    }

    @JmsListener(destination = queue, containerFactory = "connectionFactory")
    public void receiveDeleteUser(UserEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("deleteUser")) {
            Integer idUser = enveloppe.getUserId();
            userService.deleteUser(idUser);
        }
    }
    
    @JmsListener(destination = queue, containerFactory = "connectionFactory")
    public void receiveGetUser(UserEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("getUser")) {
            Integer idUser = enveloppe.getUserId();
            userService.getUser(idUser);
        }
    }
    
    @JmsListener(destination = queue, containerFactory = "connectionFactory")
    public void receiveGetAllUsers(UserEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("getAllUsers")) {
            userService.getAllUsers();
        }
    }
    
//    @JmsListener(destination = queue, containerFactory = "connectionFactory")
//    public void receiveGetUserByLoginPwd(UserEnveloppe enveloppe) {
//        if (enveloppe.getAction().equals("getUserByLoginPwd")) {
//        	String login = enveloppe.getUser().getLogin();
//        	String pwd = enveloppe.getUser().getLogin();
//            userService.getUserByLoginPwd(login,pwd);
//        }
//    }
}
