package fr.asi.user.servicebus;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.asi.user.model.UserModel;
import fr.asi.user.model.UserEnveloppe;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class UserSBModule {
    
    private static final String queue = "Endpoint=sb://asi2groupe5.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=flrDi447tRkVp2X24LaJEaU/sg9uwaG9zFXmA3tYGqM=";

    @Autowired
    private JmsTemplate jmsTemplate;
    public void getUser (int userId) {
    	UserEnveloppe message = new UserEnveloppe("getUser",userId);
    	jmsTemplate.convertAndSend(queue,message);
    }
    
    public void addUser (UserModel userModel) {
    	UserEnveloppe message = new UserEnveloppe("addUser", userModel);
    	jmsTemplate.convertAndSend(queue, message);
    }
    
    public void updateUser (UserModel userModel) {
    	UserEnveloppe message = new UserEnveloppe("updateUser",userModel);
    	jmsTemplate.convertAndSend(queue, message);
    }

    public void deleteUser (int userId) {
    	UserEnveloppe message = new UserEnveloppe("deleteUser",userId);
    	jmsTemplate.convertAndSend(queue, message);
    }
    
    public void getAllUsers() {
    	UserEnveloppe message = new UserEnveloppe("getAllUsers");
    	jmsTemplate.convertAndSend(queue, message);
    }
    
//    public void getUserByLoginPwd(int id, String login) {
//    	UserModel userModel = new UserModel(id,login);
//    	UserEnveloppe message = new UserEnveloppe("getUserByLoginPwd", userModel);
//    	jmsTemplate.convertAndSend(queue, message);
//    }
}