package fr.asi.user.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.asi.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.asi.user.model.UserModel;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public List<UserModel> getAllUsers() {
		List<UserModel> userList = new ArrayList<>();
		userRepository.findAll().forEach(userList::add);
		return userList;
	}

	public Optional<UserModel> getUser(String login) {
		return userRepository.findByLogin(login);
	}

	public Optional<UserModel> getUser(Integer id) {
		return userRepository.findById(id);
	}

	public void addUser(UserModel user) {
		userRepository.save(user);
	}

	public void updateUser(UserModel user) {
		userRepository.save(user);
	}

	public void deleteUser(Integer id) {
		userRepository.deleteById(id);
	}

	public Integer authUser(UserModel user) {
		UserModel userToRetrieve = userRepository.findByLogin(user.getLogin()).orElseGet(UserModel::new);

		assert userToRetrieve.getPassword() != null;
		if (user.getPassword().equals(userToRetrieve.getPassword())) {
			return userToRetrieve.getId();
		}
		return -1;
	}
}
