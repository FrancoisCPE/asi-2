package fr.asi.user.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import fr.asi.dto.CardDTO;
import fr.asi.dto.UserDTO;
import fr.asi.user.model.ModelMapper;
import fr.asi.user.service.UserService;
import org.apache.activemq.util.IntSequenceGenerator;
import org.apache.catalina.User;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.asi.user.model.UserModel;

@CrossOrigin
@RestController
public class UserRestController {

	@Autowired
	ModelMapper mapper;

	@Autowired
	private UserService userService;

	@RequestMapping("/api/users")
	private List<UserDTO> getAllUsers() {
		List<UserDTO> usersDTO = new ArrayList<>();
		for (UserModel u : userService.getAllUsers()) {
			usersDTO.add(mapper.modelToDto(u));
		}

		return usersDTO;
	}

	@RequestMapping("/api/user/{id}")
	private UserDTO getUser(@PathVariable Integer id) {
		Optional<UserModel> optionalUser = userService.getUser(id);
		UserModel user = optionalUser.orElseGet(UserModel::new);
		return mapper.modelToDto(user);
	}

	@RequestMapping("/api/user/login/{login}")
	private UserDTO getUser(@PathVariable String login) {
		Optional<UserModel> optionalUser = userService.getUser(login);
		UserModel user = optionalUser.orElseGet(UserModel::new);
		return mapper.modelToDto(user);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/user")
	public void addUser(@RequestBody UserModel user) {
		userService.addUser(user);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/api/user/{id}")
	public void updateUser(@RequestBody UserModel user, @PathVariable Integer id) {
		user.setId(id);
		userService.updateUser(user);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/api/user/{id}")
	public void deleteUser(@PathVariable Integer id) {
		userService.deleteUser(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/user/auth")
	public Integer authUser(@RequestBody UserModel user) {
		return userService.authUser(new UserModel(user.getLogin(), user.getPassword()));
	}
}
