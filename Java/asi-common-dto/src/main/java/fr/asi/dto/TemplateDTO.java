package fr.asi.dto; 

public class TemplateDTO {
    private Integer templateId;
    private String name;
    private String imgUrl;
    private Integer pv;

    public TemplateDTO(Integer templateId, String name, String imgUrl, Integer pv, double price) {
        this.templateId = templateId;
        this.name = name;
        this.imgUrl = imgUrl;
        this.pv = pv;
        this.price = price;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Integer getPv() {
        return pv;
    }

    public void setPv(Integer pv) {
        this.pv = pv;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    private double price;
}
