package fr.asi.dto;

public class StoreDTO {
	private int userId;
	private int cardId;

	public StoreDTO() {
	}

	public int getUserId() {
		return userId;
	}

	public void setuserId(int userId) {
		this.userId = userId;
	}

	public int getcardId() {
		return cardId;
	}

	public void setcardId(int cardId) {
		this.cardId = cardId;
	}
}
