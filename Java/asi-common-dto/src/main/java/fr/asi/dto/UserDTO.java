package fr.asi.dto; 

public class UserDTO {
	

	private Integer id;
	private String login;
	private float account;
	private String lastName;
	private String surName;
	private String email;
	
	public UserDTO() {};

	public UserDTO(Integer id, String login) {
		this.id= id;
		this.login = login;
		this.lastName="lastname_default";
		this.surName="surname_default";
		this.email="email_default";
	}

	public UserDTO(Integer id, String login, float account, String lastName, String surName, String email) {
		this.id = id;
		this.login = login;
		this.account = account;
		this.lastName = lastName;
		this.surName = surName;
		this.email = email;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public float getAccount() {
		return account;
	}

	public void setAccount(float account) {
		this.account = account;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
	
