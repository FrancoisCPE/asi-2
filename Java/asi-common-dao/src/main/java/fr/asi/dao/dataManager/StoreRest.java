package fr.asi.dao.dataManager;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.asi.dto.StoreDTO;

public interface StoreRest {
	String SERVER = "store";
	Integer PORT = 8080;
	String BASE_URL = "/api";

	// URI
	String BUY_CARD = "/buy";
	String SELL_CARD = "/sell";

	@RequestMapping(value = BASE_URL + BUY_CARD, method = RequestMethod.POST)
	ResponseEntity<Boolean> buyCard(@RequestBody StoreDTO store);

	@RequestMapping(value = BASE_URL + SELL_CARD, method = RequestMethod.POST)
	ResponseEntity<Boolean> sellCard(@RequestBody StoreDTO store);

}
