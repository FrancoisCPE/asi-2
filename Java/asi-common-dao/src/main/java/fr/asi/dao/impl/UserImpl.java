package fr.asi.dao.impl;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.apache.catalina.User;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import fr.asi.dao.dataManager.UserRest;
import fr.asi.dto.UserDTO; 


public class UserImpl implements UserRest {
	
	private final RestTemplate restTemplate;

	public UserImpl() {
		this.restTemplate = new RestTemplate();
	}
	
	public UserImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	public ResponseEntity<List<UserDTO>> getAllUsers() {
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(UserRest.SERVER)
				.port(UserRest.PORT)
				.path(UserRest.BASE_URL + UserRest.GET_USERS)
				.build();

		ResponseEntity<UserDTO[]> reUsers = restTemplate.getForEntity(uriComponents.toUri(), UserDTO[].class);
		return new ResponseEntity<>(List.of(Objects.requireNonNull(reUsers.getBody())), reUsers.getStatusCode());
	}

	public ResponseEntity<UserDTO> getUser(Integer id) {
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(UserRest.SERVER)
				.port(UserRest.PORT)
				.path(UserRest.BASE_URL + UserRest.GET_USER_BY_ID)
				.buildAndExpand(id);

		ResponseEntity<UserDTO> reUser = restTemplate.getForEntity(uriComponents.toUri(), UserDTO.class);
		return new ResponseEntity<>(reUser.getBody(), reUser.getStatusCode());
	}

	public ResponseEntity<UserDTO> getUser(String login) {
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(UserRest.SERVER)
				.port(UserRest.PORT)
				.path(UserRest.BASE_URL + UserRest.GET_USER_BY_ID)
				.buildAndExpand(login);

		ResponseEntity<UserDTO> reUser = restTemplate.getForEntity(uriComponents.toUri(), UserDTO.class);
		return new ResponseEntity<>(reUser.getBody(), reUser.getStatusCode());
	}

	public ResponseEntity<HttpStatus> addUser(UserDTO user) {
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(UserRest.SERVER)
				.port(UserRest.PORT)
				.path(UserRest.BASE_URL + UserRest.ADD_USER)
				.build();

		ResponseEntity<HttpStatus> reUser = restTemplate.postForEntity(uriComponents.toUri(), user, HttpStatus.class);
		return new ResponseEntity<>(reUser.getBody(), reUser.getStatusCode());
	}

	public void updateUser(UserDTO user, Integer id) {
		 UriComponents uriComponents = UriComponentsBuilder
				 .newInstance()
				 .scheme("http")
				 .host(UserRest.SERVER)
				 .port(UserRest.PORT)
				 .path(UserRest.BASE_URL + UserRest.UPDATE_USER)
				 .buildAndExpand(id);

		restTemplate.put(uriComponents.toUri(), user);
	}

	public void deleteUser(Integer id) {
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
	    	    .scheme("http")
				.host(UserRest.SERVER)
				.port(UserRest.PORT)
				.path(UserRest.BASE_URL + UserRest.DELETE_USER)
				.buildAndExpand(id);

		restTemplate.delete(uriComponents.toUri());
	}

	public ResponseEntity<Integer> authUser(UserDTO user) {
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(UserRest.SERVER)
				.port(UserRest.PORT)
				.path(UserRest.BASE_URL + UserRest.GET_USER_BY_ID)
				.build();

		ResponseEntity<HttpStatus> reUser = restTemplate.postForEntity(uriComponents.toUri(), user, HttpStatus.class);
		return new ResponseEntity<>(Objects.requireNonNull(reUser.getBody()).value(), reUser.getStatusCode());
	}
}