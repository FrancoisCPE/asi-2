package fr.asi.dao.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import fr.asi.dao.dataManager.CardRest;
import fr.asi.dto.CardDTO;

public class CardImpl implements CardRest{
	
	private final RestTemplate restTemplate;

	public CardImpl() {
		this.restTemplate = new RestTemplate();
	}

	public CardImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	// Get all cards
	public ResponseEntity<List<CardDTO>> getAllCards(){
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(CardRest.SERVER)
				.port(CardRest.PORT)
				.path(CardRest.BASE_URL + CardRest.GET_CARDS)
				.build();

		ResponseEntity<CardDTO[]> reCards = restTemplate.getForEntity(uriComponents.toUri(), CardDTO[].class);
		return new ResponseEntity<>(List.of(Objects.requireNonNull(reCards.getBody())), reCards.getStatusCode());
	}

	// Get card by id
	public ResponseEntity<CardDTO> getCard(Integer id){
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(CardRest.SERVER)
				.port(CardRest.PORT)
				.path(CardRest.BASE_URL + CardRest.GET_CARD_BY_ID)
				.buildAndExpand(id);

		ResponseEntity<CardDTO> reCard = restTemplate.getForEntity(uriComponents.toUri(), CardDTO.class);
		return new ResponseEntity<>(reCard.getBody(),reCard.getStatusCode());
	}

	// Add card
	public ResponseEntity<HttpStatus> addCard(CardDTO card){
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(CardRest.SERVER)
				.port(CardRest.PORT)
				.path(CardRest.BASE_URL + CardRest.ADD_CARD)
				.build();

		ResponseEntity<HttpStatus> reCard = restTemplate.postForEntity(uriComponents.toUri(), card, HttpStatus.class);
		return new ResponseEntity<>(reCard.getBody(), reCard.getStatusCode());
	}

	// Update card
	public void updateCard(CardDTO card, Integer id){
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(CardRest.SERVER)
				.port(CardRest.PORT)
				.path(CardRest.BASE_URL + CardRest.UPDATE_CARD)
				.buildAndExpand(id);

		restTemplate.put(uriComponents.toUri(), card);
	}

	// Delete card
	public void deleteCard(Integer id){
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(CardRest.SERVER)
				.port(CardRest.PORT)
				.path(CardRest.BASE_URL + CardRest.DELETE_CARD)
				.buildAndExpand(id);

		restTemplate.delete(uriComponents.toUri());
	}

	// Get all cards to sell (with userId = 0)
	public ResponseEntity<List<CardDTO>> getCardsByUserId(Integer id){
		UriComponents uriComponents = UriComponentsBuilder
				.newInstance()
				.scheme("http")
				.host(CardRest.SERVER)
				.port(CardRest.PORT)
				.path(CardRest.BASE_URL + CardRest.GET_CARDS_BY_USER_ID)
				.buildAndExpand(id);

		ResponseEntity<CardDTO> reCards = restTemplate.getForEntity(uriComponents.toUri(), CardDTO.class);
		return new ResponseEntity<>(List.of(Objects.requireNonNull(reCards.getBody())), reCards.getStatusCode());
	}
}


