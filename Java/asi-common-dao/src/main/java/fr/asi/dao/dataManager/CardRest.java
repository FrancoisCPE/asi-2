package fr.asi.dao.dataManager;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.asi.dto.CardDTO;

public interface CardRest {
	String SERVER = "card";
	Integer PORT = 8080;
	String BASE_URL = "/api";

	// URI
	String GET_CARDS = "/cards";
	String GET_CARD_BY_ID = "/card/{id}";
	String ADD_CARD = "/card";
	String UPDATE_CARD = "/card/{id}";
	String DELETE_CARD = "/card/{id}";
	String GET_CARDS_BY_USER_ID = "/api/cards_user/{id}";

	@RequestMapping(value = BASE_URL + GET_CARDS, method = RequestMethod.GET)
	ResponseEntity<List<CardDTO>> getAllCards();

	@RequestMapping(value = BASE_URL + GET_CARD_BY_ID, method = RequestMethod.GET)
	ResponseEntity<CardDTO> getCard(@PathVariable Integer id);

	@RequestMapping(value = BASE_URL + ADD_CARD, method = RequestMethod.POST)
	ResponseEntity<HttpStatus> addCard(@RequestBody CardDTO card);

	@RequestMapping(value = BASE_URL + UPDATE_CARD, method = RequestMethod.PUT)
	void updateCard(@RequestBody CardDTO card, @PathVariable Integer id);

	@RequestMapping(value = BASE_URL + DELETE_CARD, method = RequestMethod.DELETE)
	void deleteCard(@PathVariable Integer id);

	@RequestMapping(value = BASE_URL + GET_CARDS_BY_USER_ID, method = RequestMethod.GET)
	ResponseEntity<List<CardDTO>> getCardsByUserId(@PathVariable Integer id);
}
