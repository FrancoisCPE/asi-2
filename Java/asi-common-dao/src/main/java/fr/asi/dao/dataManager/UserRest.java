package fr.asi.dao.dataManager;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.asi.dto.UserDTO;

public interface UserRest {
	String SERVER = "user";
	Integer PORT = 8080;
	String BASE_URL = "/api";

	// URI
	String GET_USERS = "/users";
	String GET_USER_BY_ID = "/user/{id}";
	String GET_USER_BY_LOGIN = "/user/login/{login}";
	String ADD_USER = "/user";
	String UPDATE_USER = "/user/{id}";
	String DELETE_USER = "/user/{id}";
	String AUTH = "/user/auth";

	@RequestMapping(value = BASE_URL + GET_USERS, method = RequestMethod.GET)
	ResponseEntity<List<UserDTO>> getAllUsers();

	@RequestMapping(value = BASE_URL + GET_USER_BY_ID, method = RequestMethod.GET)
	ResponseEntity<UserDTO> getUser(@PathVariable Integer id);

	@RequestMapping(value = BASE_URL + GET_USER_BY_LOGIN, method = RequestMethod.GET)
	ResponseEntity<UserDTO> getUser(@PathVariable String login);

	@RequestMapping(value = BASE_URL + ADD_USER, method = RequestMethod.POST)
	ResponseEntity<HttpStatus> addUser(@RequestBody UserDTO user);

	@RequestMapping(value = BASE_URL + UPDATE_USER, method = RequestMethod.PUT)
	void updateUser(@RequestBody UserDTO user, @PathVariable Integer id);

	@RequestMapping(value = BASE_URL + DELETE_USER, method = RequestMethod.DELETE)
	void deleteUser(@PathVariable Integer id);

	@RequestMapping(value = BASE_URL + AUTH, method = RequestMethod.POST)
	ResponseEntity<Integer> authUser(@RequestBody UserDTO user);
}
