package fr.asi.card.servicebus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import fr.asi.card.model.CardModel;
import fr.asi.card.model.CardEnveloppe;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class CardSBModule {
    private static final String queue = "CardsQueue";

    @Autowired
    private JmsTemplate jmsTemplate;
    public void getCard (int cardId) {
        CardEnveloppe message = new CardEnveloppe("getCard",cardId);
        jmsTemplate.convertAndSend(queue,message);
    }

    public void addCard (CardModel cardModel) {
        CardEnveloppe message = new CardEnveloppe("addCard", cardModel);
        jmsTemplate.convertAndSend(queue, message);
    }

    public void updateCard (CardModel cardModel) {
        CardEnveloppe message = new CardEnveloppe("updateCard",cardModel);
        jmsTemplate.convertAndSend(queue, message);
    }

    public void updateCardRef (CardModel cardModel) {
        CardEnveloppe message = new CardEnveloppe("updateCardRef",cardModel);
        jmsTemplate.convertAndSend(queue, message);
    }

    public void deleteCardModel (int cardId) {
        CardEnveloppe message = new CardEnveloppe("deleteCardModel",cardId);
        jmsTemplate.convertAndSend(queue, message);
    }

    public void getAllCardModel() {
        CardEnveloppe message = new CardEnveloppe("getAllCardModel");
        jmsTemplate.convertAndSend(queue, message);
    }


}
