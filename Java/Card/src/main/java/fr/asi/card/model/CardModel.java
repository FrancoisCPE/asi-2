package fr.asi.card.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
public class CardModel extends CardReference{
	private float energy;
	private float hp;
	private float defence;
	private float attack;
	private float price;
	private int userId;

	public CardModel() {
		super();
	}

	public CardModel(CardReference cardRef) {
		super(cardRef);
	}

	public CardModel(String name, String description, String family, String affinity, float energy, float hp, float defence, float attack,String imgUrl,String smallImg,float price, int userId) {
		super(name, description, family, affinity,imgUrl,smallImg);
		this.energy = energy;
		this.hp = hp;
		this.defence = defence;
		this.attack = attack;
		this.price=price;
		this.userId=userId;
	}
	public float getEnergy() {
		return energy;
	}
	public void setEnergy(float energy) {
		this.energy = energy;
	}
	public float getHp() {
		return hp;
	}
	public void setHp(float hp) {
		this.hp = hp;
	}
	public float getDefence() {
		return defence;
	}
	public void setDefence(float defence) {
		this.defence = defence;
	}
	public float getAttack() {
		return attack;
	}
	public void setAttack(float attack) {
		this.attack = attack;
	}

	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}

	public float computePrice() {
		return this.hp * 20 + this.defence*20 + this.energy*20 + this.attack*20;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}


}
