package fr.asi.card.model;

import org.springframework.stereotype.Service;

import fr.asi.card.model.CardModel;
import fr.asi.dto.CardDTO;

@Service
public class ModelMapper {

	public CardModel dtoToModel(CardDTO cardDto) {
		return new CardModel(cardDto.getName(), cardDto.getDescription(), cardDto.getFamily(), cardDto.getAffinity(),
				cardDto.getEnergy(), cardDto.getHp(), cardDto.getDefence(), cardDto.getAttack(), cardDto.getImgUrl(),
				cardDto.getSmallImgUrl(), cardDto.getPrice(), cardDto.getUserId());

	}

	public CardDTO modelToDto(CardModel cardmodel) {
		return new CardDTO(cardmodel.getId(), cardmodel.getName(), cardmodel.getDescription(), cardmodel.getFamily(),
				cardmodel.getAffinity(), cardmodel.getImgUrl(), cardmodel.getSmallImgUrl(), cardmodel.getEnergy(),
				cardmodel.getHp(), cardmodel.getDefence(), cardmodel.getAttack(), cardmodel.getPrice(),
				cardmodel.getUserId());
	}
}
