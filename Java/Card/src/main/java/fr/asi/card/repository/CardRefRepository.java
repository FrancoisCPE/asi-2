package fr.asi.card.repository;

import org.springframework.data.repository.CrudRepository;

import fr.asi.card.model.CardReference;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRefRepository extends CrudRepository<CardReference, Integer> {

}
