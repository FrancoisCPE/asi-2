package fr.asi.card.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import fr.asi.card.repository.CardRefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.asi.card.model.CardModel;
import fr.asi.card.model.CardReference;
import fr.asi.card.repository.CardModelRepository;

@Service
public class CardModelService {
	
	private Random rand;

	public CardModelService() {
		this.rand=new Random();
	}


	@Autowired
	private CardModelRepository cardRepository;

	@Autowired
	private CardRefRepository cardRefRepository;
	
	@Autowired
	private CardReferenceService cardRefService;

	public List<CardModel> getAllCardModel() {
		List<CardModel> cardList = new ArrayList<>();
		cardRepository.findAll().forEach(cardList::add);
		return cardList;
	}

	public void addCard(CardModel cardModel) {
		cardRepository.save(cardModel);
	}

	public void updateCardRef(CardModel cardModel) {
		cardRefRepository.save(cardModel);
	}

	public void updateCard(CardModel cardModel) {
		cardRepository.save(cardModel);
	}

	public Optional<CardModel> getCard(Integer id) {
		return cardRepository.findById(id);
	}
	
	public void deleteCardModel(Integer id) {
		cardRepository.deleteById(id);
	}
	
	public List<CardModel> getRandCard(int nbr){
		List<CardModel> cardList=new ArrayList<>();
		for(int i=0;i<nbr;i++) {
			CardReference currentCardRef=cardRefService.getRandCardRef();
			CardModel currentCard=new CardModel(currentCardRef);
			currentCard.setAttack(rand.nextFloat()*100);
			currentCard.setDefence(rand.nextFloat()*100);
			currentCard.setEnergy(100);
			currentCard.setHp(rand.nextFloat()*100);
			currentCard.setPrice(111);
			//save new card before sending for user creation
			//this.addCard(currentCard);
			cardList.add(currentCard);
		}
		return cardList;
	}


	public List<CardModel> getCardByUserId(Integer id){
		return this.cardRepository.findByUserId(id);
	}
}

