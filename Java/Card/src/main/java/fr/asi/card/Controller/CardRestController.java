package fr.asi.card.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import fr.asi.card.model.ModelMapper;
import fr.asi.card.service.CardModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.asi.card.model.CardModel;
import fr.asi.dto.CardDTO;

@CrossOrigin
@RestController
public class CardRestController {

	
	@Autowired
	ModelMapper mapper;
	
	@Autowired
	private CardModelService cardModelService;
	
	@RequestMapping("/api/cards")
	private List<CardDTO> getAllCards() {
		List<CardDTO> cardsDTO=new ArrayList<>();
		for(CardModel c:cardModelService.getAllCardModel()){
			cardsDTO.add(mapper.modelToDto(c));
		}

		return cardsDTO;
	}
	
	@RequestMapping("/api/card/{id}")
	private CardDTO getCard(@PathVariable Integer id) {
		Optional<CardModel> optionalCard = cardModelService.getCard(id);
		CardModel card = optionalCard.orElseGet(CardModel::new);
		return mapper.modelToDto(card);
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/api/card")
	public void addCard(@RequestBody CardModel card) {
		cardModelService.addCard(card);
	}
	
	@RequestMapping(method=RequestMethod.PUT,value="/api/card/{id}")
	public void updateCard(@RequestBody CardModel card,@PathVariable Integer id) {
		card.setId(id);
		cardModelService.updateCard(card);
		cardModelService.updateCardRef(card);
	}
	
	@RequestMapping(method=RequestMethod.DELETE,value="/api/card/{id}")
	public void deleteCard(@PathVariable Integer id) {
		cardModelService.deleteCardModel(id);
	}

	@RequestMapping("/api/cards_user/{id}")
	private List<CardDTO> getCardsByUserId(@PathVariable Integer id) {
		List<CardDTO> cardsDTO = new ArrayList<>();
		for(CardModel c:cardModelService.getCardByUserId(id)){
			cardsDTO.add(mapper.modelToDto(c));
		}

		return cardsDTO;
	}
}
