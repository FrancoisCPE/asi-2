package fr.asi.card.model;

import fr.asi.card.model.CardModel;

public class CardEnveloppe {

	public CardEnveloppe(String action) {
		Action = action;
	}

	public CardEnveloppe() {
	}

	private static final long serialVersionUID = 2L;

	private String Action;
	private CardModel card;
	private Integer cardId;
	
	public CardEnveloppe(String action, CardModel card)
	{
		Action = action;
		this.card = card;
	}
	
	public CardEnveloppe(String action, Integer cardId) {
		Action = action; 
		this.cardId = cardId;
	}
	
	public String getAction() {
		return Action;
	}

	public void setAction(String action) {
		Action = action;
	}

	public CardModel getCard() {
		return card;
	}

	public void setCard(CardModel card) {
		this.card = card;
	}

	public Integer getCardId() {
		return cardId;
	}

	public void setCardId(Integer cardId) {
		this.cardId = cardId;
	}

	@Override
	public String toString() {
		return String.format("{action:\"%s\", card:\"%s\", cardId: \"%d\"}", Action, card, cardId);
	}
}
