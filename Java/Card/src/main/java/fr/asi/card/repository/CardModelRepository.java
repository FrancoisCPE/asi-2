package fr.asi.card.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import fr.asi.card.model.CardModel;
import org.springframework.stereotype.Repository;

@Repository
public interface CardModelRepository extends CrudRepository<CardModel, Integer> {
    List<CardModel> findByUserId(Integer uId);
}
