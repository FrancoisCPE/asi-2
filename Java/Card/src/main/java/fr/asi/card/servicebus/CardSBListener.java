package fr.asi.card.servicebus;

import fr.asi.card.service.CardModelService;
import fr.asi.card.model.CardEnveloppe;
import fr.asi.card.model.CardModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;

public class CardSBListener {

	@Autowired
	JmsTemplate jmsTemplate;
	
	@Autowired
	private CardModelService cardModelService;
	

    @JmsListener(destination = "CardsQueue", containerFactory = "connectionFactory")
    public void receiveAddCard(CardEnveloppe enveloppe) {
        System.out.println(enveloppe);
        if (enveloppe.getAction().equals("addCard")) {
            CardModel cardModel = enveloppe.getCard();
            cardModelService.addCard(cardModel);
        }
    }

    @JmsListener(destination = "CardsQueue", containerFactory = "connectionFactory")
    public void receiveUpdateCard(CardEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("updateCard")) {
            CardModel cardModel = enveloppe.getCard();
            cardModelService.updateCard(cardModel);
        }
    }
    
    @JmsListener(destination = "CardsQueue", containerFactory = "connectionFactory")
    public void receiveUpdateCardRef(CardEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("updateCardRef")) {
            CardModel cardModel = enveloppe.getCard();
            cardModelService.updateCardRef(cardModel);
        }
    }

    @JmsListener(destination = "CardsQueue", containerFactory = "connectionFactory")
    public void receiveDeleteCardModel(CardEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("deleteCardModel")) {
            Integer idCard = enveloppe.getCardId();
            cardModelService.deleteCardModel(idCard);
        }
    }
    
    @JmsListener(destination = "CardsQueue", containerFactory = "connectionFactory")
    public void receiveGetCard(CardEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("getCard")) {
            Integer idCard = enveloppe.getCardId();
            cardModelService.getCard(idCard);
        }
    }
    
    @JmsListener(destination = "CardsQueue", containerFactory = "connectionFactory")
    public void receiveGetAllCardModel(CardEnveloppe enveloppe) {
        if (enveloppe.getAction().equals("getAllCardModel")) {
            cardModelService.getAllCardModel();
        }
    }
}
