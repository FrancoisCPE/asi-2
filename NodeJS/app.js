const { request } = require('express')
const express = require('express')
const app = express()

var cors = require('cors');
app.use(cors());
//set the template engine ejs
app.set('view engine', 'ejs')

//middlewares
app.use(express.static('public'))


//routes
app.get('/', (req, res) => {
    res.render('index')
})

//Listen on port 3000
server = app.listen(3000)



//socket.io instantiation
const io = require("socket.io")(server)


//listen on every connection
io.on('connection', (socket) => {
    console.log('New user connected')

    //default username
    socket.username = "Anonymous"

    //listen on change_username
    socket.on('change_username', (data) => {
        socket.username = data.username
    })

    //listen on new_message
    socket.on('new_message', (data) => {
        //broadcast the new message
        io.sockets.emit('new_message', { message: data.message, username: socket.username });
    })
})

function getAllUsers() {
    return new Promise(resolve => {
        request(options, function(error, response, result) {
            if (error) {
                console.log(error);
            } else {
                resolve(result)
            }
        });
    });
}

function getAllUsersJS() {
    return getAllUsers().then((result) => {
        return result
    });
}

function logUser(userId) {
    return getAllUsersJS().then(json => {
        var currentUser;
        Object.keys(json).forEach(function(key) {
            if (userId === json[key].id) {
                console.log("Bienvenue" + json[key].surName + " !")
                currentUser = json[key]
            }
        })
        return new Promise(resolve => {
            resolve => { resolve(currentUser) };
        })
    })
}