$(function () {
    //make connection
    var socket = io.connect('http://localhost:3000')

    //buttons and inputs
    var message = $("#message")
    var username = $("#username")
    var send_message = $("#send_message")
    var send_username = $("#send_username")
    var chatroom = $("#chatroom")
    var feedback = $("#feedback")
    var listUsers = $("#listUsers")


    // var listUsersDisplay = $("#listUsersDisplay")
    //var update_users_list = $("#listUsersDisplay")
    listUsers.append("Users List : ");
    fetch('http://user:8080/api/users')
        .then(res => res.json())
        .then(data => data.forEach(user => {
            listUsers.append(user.login + " - ")
        }))

    //Emit message
    send_message.click(function () {
        socket.emit('new_message', { message: message.val() })
    })

    //Listen on new_message
    socket.on("new_message", (data) => {
        feedback.html('');
        message.val('');
        chatroom.append("<p class='message'>" + data.username + ": " + data.message + "</p>")
    })

    //Emit a username
    send_username.click(function () {
        socket.emit('change_username', { username: username.val() })
    })

    message.keydown(function (e) {
        if (e.which == 13) {
            socket.emit('new_message', { message: message.val() })
        }
    });
});