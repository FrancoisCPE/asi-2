# Atelier ASI-2

Nasri Galdeano - Thomas Oudard - François Portaz - Antonin Poulard 

## Suivi des scéances

|Séances|Nasri                              |Thomas                                     |François                           |Antonin                         |
|:-----:|:---------------------------------:|:-----------------------------------------:|:---------------------------------:|:------------------------------:|
| atelier 1                                                                                                                                                  |
|28/09  |Diagramme de classe monolithique   |Diagramme de classe monolithique           |Diagramme de classe monolithique   |Diagramme de classe monolithique|
|29/09  |Architecture micro-service         |Architecture micro-service                 |Architecture micro-service         |Architecture micro-service      |
|15/11  |Front - ReactJS                    |Docker-compose                             |Librairies DTO                     | Tableau comparatif des framwork|
| atelier 2                                                                                                                                                  |
|16/11  |Front - ReactJS                    |Recherche Nexus3 + Aide François           |Librairies DAO                     | Mise en place du chat          |
|17/11  |Front - ReactJS                    |Création du micro-service auth             |Modification des classes existantes|Mise en place de l'ESB          |
|18/11  |Front - ReactJS                    |Création du micro-service auth             |Modification des classes existantes|Mise en place de l'ESB          |
|13/12  |Absent                             |Absent  |Finalisation du service Store + connexion front-back|Mise en place de l'ESB                            |
|13/12  |Connexion front-back + aide Thomas |pipeline CI/CD                             |Correction de bugs sur les services|Programmation du chat Node.js   |
| atelier 3                                                                                                                                                  |
|15/12  |Connexion front-back + aide Thomas |pipeline CI/CD  (+test covid cas contact)  |Correction de bug spring-boot      |Programmation du chat Node.js   |

Réalisations sur cette 2ème session:

Thomas :
- Passage du script sql en Init var par Springboot
- Debug du docker-compose
- Finalisation de la chaîne CI/CD avec un registry d'image docker
- Debug de la connexion service -> BDD
- Debug/Refactor librairie DAO
- Refactor librairie DTO
- Debug/Refactor/Unification du service Auth et User et ajout d'une sécurité supplémentaire
- Debug/Refactor du service Card
- Debug/Refactor du service Store
- Mise en place et vérification du retour des DTO pour chaque requete
- Mise en place des nouveaux EndPoints
- Aide NodeJS

Antonin : 
- ESB
- Aides succintes sur un ou deux sujets Java
- Aide NodeJS

Nasri :
- Finalisation Front
- Jointure du front au back

Francois :
- Node JS (chat) récupération des utilisateurs + affichage
- Aide debug services Java

## Éléments réalisés

|Éléments           |Réalisé                                                                                                      |
|:-----------------:|:-----------------------------------------------------------------------------------------------------------:|
|Docker             |Terminé et fonctionnel + ajout automatique des données de test dans la BDD PostGreSQL                        |
|CI/CD              |Terminé CI/CD GitLab entièrement fonctionnelle (build + Hébergement des images docker)                       |
|Front              |Terminé                                                                                                      |
|Micro-service User |Terminé et utilisation de BDD postgre                                                                        |
|Micro-service Card |Terminé et utilisation de BDD postgre                                                                        |
|Micro-service Store|Terminé et utilisation de BDD postgre                                                                        |
|Node.js            |Chat général (discussion avec tous les users) + récupération et affichages des utilisateurs                  |
|ESB                |Codé mais erreur sur le broker activemq et pas de solution trouvée (branche SBBuildBranch)                   |
## Vidéos youtube

https://drive.google.com/drive/folders/1kD13WXBwITHyh0rvUpW4mR1CVjhelZ4m?usp=sharing

## Éléments non-réalisés

|Éléments            |
|:------------------:|
|Combats             |
|Chat perso          |

## Problèmes bloquants du projet
 - Problème avec le broker ("message": "Uncategorized exception occurred during JMS processing; nested exception is javax.jms.JMSException: Could not connect to broker URL: tcp://localhost:61616. Reason: java.net.ConnectException: Connection refused (Connection refused)") pour laquelle nous n'avons pas trouvé de solution. (Service Bus implémenté uniquement dans le controller de Card sur la branche SBBuildBranch car non-fonctionnel donc pas de nécéssité de l'implémenter ailleurs)

- Problème avec les messages du chat (cors sur le socket.io). On a essayé d'ajouter la dépendance cors au package node mais le problème persiste. Cependant, le chat est fonctionel avec un lancement en local.

Doc lancement chat local nodejs : 
1- changer l'url du fetch de http://user:8080/ pour http://localhost:84/ dans le fichier NodeJS/public/chat.js
2- ```npm install``` dans le dossier NodeJS
3- ```npm start``` dans le dossier NodeJS
4- lancer un navigateur à l'adresse : http://localhost:3000/

## Doc de lancemement pour tester 
 - Cloner ce projet
 - Se placer à l'intérieur de celui-ci
 - Lancer la commande `./start.sh`. Avant cela, vérifier que les ports 8080 81 82 83 84 sont bien libres.
 - attendre le lancement automatique des services
 - Se placer dans le dossier cardapp_frontend : `cd cardapp_frontend`
 - Lancer la commande `yarn install` puis `yarn start`
 
## Liens utile

[Gitlab repo](https://gitlab.com/FrancoisCPE/asi-2/-/tree/main)
